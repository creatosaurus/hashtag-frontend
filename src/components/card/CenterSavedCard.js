import React, { useState, useContext } from 'react'
import constant from '../../constant'
import axios from 'axios'
import { toast } from 'react-toastify';
import AppContext from '../../store/DataProvider'
import '../cardcss/CenterSavedCard.css'

const CenterSavedCard = ({ data }) => {

    const context = useContext(AppContext)
    const [activeId, setactiveId] = useState(null)
    const [deleteTagId, setdeleteTagId] = useState(null)
    const month = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sept", "Oct", "Nov", "Dec"];

    const toggleId = (id) => {
        if (activeId === id) {
            setactiveId(null)
        } else {
            setactiveId(id)
        }
    }

    const deleteTheHashTags = async (id) => {
        try {
            if (!context.canEdit) return toast("You have only view permission")
            setdeleteTagId(id)
            await axios.delete(constant.url + id)

            toast("Tag's deleted successfully", {
                autoClose: 1000,
                hideProgressBar: false,
            });
            setdeleteTagId(null)
            context.removeDeletedHashtag(id)
        } catch (error) {
            setdeleteTagId(null)
        }
    }

    const copyTags = (tags) => {
        let copyText = document.getElementById("copySaved");
        let concatTags
        tags.forEach((data, index) => {
            if (index === 0) {
                concatTags = data
            } else {
                concatTags = concatTags + data
            }
        })
        copyText.value = concatTags
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Tag's copied", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const edit = (groupName, tags, category, id) => {
        if (!context.canEdit) return toast("You have only view permission")
        context.changeEditId(id)
        context.changeLeftSideBarAciveButton(2)
        context.changeGroupName(groupName)
        context.changeTags(tags)
        context.changeCategory(category)
        context.changeTagsValueEdit(tags.join(" "))
    }

    const getFormatedDate = (date) => {
        date = new Date(date)
        return date.getDate() + " " + month[date.getMonth()] + " " + date.getFullYear()
    }

    return (
        <div className="center-saved-card" onClick={() => toggleId(data._id)}>
            <p>{data.groupName}</p>
            <span>{data.tags.map(data => {
                return data + " "
            })}</span>

            <div className='info'>
                <div className='row'>
                    <span>Created by: {data.userName === undefined ? "Unknown" : data.userName}</span>
                    <span>Last updated: {getFormatedDate(data.updatedAt)}</span>
                    <span className='category'>Saved in: {data.category}</span>
                </div>

                <div className='button-container'>
                    <svg onClick={() => edit(data.groupName, data.tags, data.category, data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                        <path d="M13.0207 5.82839L15.8491 2.99996L20.7988 7.94971L17.9704 10.7781M13.0207 5.82839L3.41405 15.435C3.22652 15.6225 3.12116 15.8769 3.12116 16.1421V20.6776H7.65669C7.92191 20.6776 8.17626 20.5723 8.3638 20.3847L17.9704 10.7781M13.0207 5.82839L17.9704 10.7781" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    <svg onClick={() => copyTags(data.tags)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                        <path d="M19.4 20H9.6C9.26863 20 9 19.7314 9 19.4V9.6C9 9.26863 9.26863 9 9.6 9H19.4C19.7314 9 20 9.26863 20 9.6V19.4C20 19.7314 19.7314 20 19.4 20Z" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M15 9V4.6C15 4.26863 14.7314 4 14.4 4H4.6C4.26863 4 4 4.26863 4 4.6V14.4C4 14.7314 4.26863 15 4.6 15H9" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>

                    {
                        deleteTagId === data._id ? <div className='loader' style={{ marginLeft: 20 }} /> :
                            <svg onClick={() => deleteTheHashTags(data._id)} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                <path d="M19 11V20.4C19 20.7314 18.7314 21 18.4 21H5.6C5.26863 21 5 20.7314 5 20.4V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M10 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M14 17V11" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M21 7L16 7M3 7L8 7M8 7V3.6C8 3.26863 8.26863 3 8.6 3L15.4 3C15.7314 3 16 3.26863 16 3.6V7M8 7L16 7" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                    }
                </div>
            </div>
        </div>
    )
}

export default CenterSavedCard