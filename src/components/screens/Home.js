import React, { useContext, useEffect } from 'react'
import '../screencss/Home.css'
import AppContext from '../../store/DataProvider'
import NavigationBar from '../reusableComponent/NavigationBar'
import CenterContainer from '../reusableComponent/CenterContainer'
import RightContainer from '../reusableComponent/RightContainer'
import LeftContainer from '../reusableComponent/LeftContainer'

const Home = () => {

    const context = useContext(AppContext)

    useEffect(() => {
        context.getActiveOrganization()
    }, []) // eslint-disable-line react-hooks/exhaustive-deps
    
    return (
        <div>
            <NavigationBar />
            <main className={context.leftSideBarActiveButton === 6 ? "tworow" : "threerow"}>
                <LeftContainer />
                <CenterContainer />
                <RightContainer />
            </main>
        </div>
    )
}

export default Home
