import React, { useContext, useState } from 'react'
import '../reusableComponentCss/CreateCategoryPopup.css'
import decodeToken from "jwt-decode";
import Axios from 'axios'
import AppContext from '../../store/DataProvider';
import constant from '../../constant';
import { toast } from 'react-toastify';

const CreateCategoryPopup = (props) => {

    const [loading, setloading] = useState(false)
    const [category, setcategory] = useState("")
    const context = useContext(AppContext)

    const create = async () => {
        try {
            if (category === "") return toast("Please enter the category name")
            if(context.categoryData.length === 10) toast("You can not create more than 10 category")
            setloading(true)
            const decode = decodeToken(localStorage.getItem("token"))
            let organizationId = localStorage.getItem('organizationId')

            const res = await Axios.post(`${constant.url}category/create`, {
                organizationId:organizationId,
                userId: decode.id,
                category: category.toLowerCase()
            })
            context.updateCategoryData(res.data.data)
            context.changeCategory(res.data.data.category)
            setcategory("")
            setloading(false)
            props.categoryCreatedToast()
        } catch (error) {
            setloading(false)
        }
    }

    const handelKeyDown = (e) => {
       if(e.key === "Enter") {
          create()
       }
    }

    return (
        <div className='popup-container' onClick={() => context.changeCategory("")}>
            <div className='card' onClick={(e) => e.stopPropagation()}>
                <div className='head'>
                    <span>Create new category</span>

                    <svg onClick={() => context.changeCategory("")} xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                        <path d="M6.75827 17.2426L12.0009 12M17.2435 6.75736L12.0009 12M12.0009 12L6.75827 6.75736M12.0009 12L17.2435 17.2426" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </div>

                <input
                    value={category}
                    onChange={(e) => setcategory(e.target.value)}
                    onKeyDown={handelKeyDown}
                    type="text"
                    placeholder="Enter Category Name" />
                <button onClick={loading ? null : create}>{loading ? "Creating . . ." : "Create"}</button>
            </div>
        </div>
    )
}

export default CreateCategoryPopup