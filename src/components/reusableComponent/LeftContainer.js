import React from 'react'
import LeftSideBar from './LeftSideBar'

const LeftContainer = () => {
    return (
        <div>
             <LeftSideBar /> 
        </div>
    )
}

export default LeftContainer
