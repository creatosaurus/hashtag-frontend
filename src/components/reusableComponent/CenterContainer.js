import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../reusableComponentCss/CenterContainer.css'
import Help from '../screens/Help'
import Setting from '../screens/Setting'
import CenterCreate from './CenterCreate'
import CenterDashBoard from './CenterDashBoard'
import CenterSaved from './CenterSaved'
import CenterSearch from './CenterSearch'

const CenterContainer = () => {

    const context = useContext(AppContext)

    return (
        <div className="center-dashboard-container">
            {
                context.leftSideBarActiveButton === 1 ? <CenterDashBoard /> :
                    context.leftSideBarActiveButton === 2 ? <CenterCreate /> :
                        context.leftSideBarActiveButton === 3 ? <CenterSaved /> :
                            context.leftSideBarActiveButton === 4 ? <Help /> :
                                context.leftSideBarActiveButton === 5 ? <Setting /> :
                                    context.leftSideBarActiveButton === 6 ? <CenterSearch /> : null
            }
        </div>
    )
}

export default CenterContainer
