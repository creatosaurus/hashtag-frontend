import React, { useState, useContext } from 'react'
import '../reusableComponentCss/RightCreateCategory.css'
import Axios from 'axios'
import TagsCard from './TagsCard'
import AppContext from '../../store/DataProvider'
import constant from '../../constant'
import { toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import ConnectAccountPopup from './ConnectAccountPopup'
import InstaTagsCard from './InstaTagsCard'
import Loading from './Loading'
import InsagramConnectPopup from '../popup/InsagramConnectPopup/InsagramConnectPopup'

const RightCreateCategory = () => {

    const context = useContext(AppContext)
    const [loading, setloading] = useState(false)
    const [category, setcategory] = useState("")
    const [tags, settags] = useState([])
    const [instaTags, setinstaTags] = useState([])
    const [showInstaPopup, setShowInstaPopup] = useState(false)

    const [searchResultFound, setsearchResultFound] = useState(true)

    //active buttons
    const [activeButton, setactiveButton] = useState(1)
    const [searchActiveButton, setsearchActiveButton] = useState(1)

    const [searchAccount, setsearchAccount] = useState("")
    const [popupActive, setpopupActive] = useState(false)
    const [instagramAccounts, setinstagramAccounts] = useState([])
    const [accountsLoading, setaccountsLoading] = useState(false)

    const find = async () => {
        try {
            if (!context.canEdit) return toast("You have only view permission")
            if (context.featureFactory.planId.planName === "free") return toast("You don't have any credits to search hashtags, please upgrade to search more.")
            if (category.trim() === "") return toast("Plese enter keyword to search")
            if (context.featureFactory.hashtagSearch.toString() === "0") return toast("You don't have any credits to search hashtags, please upgrade to search more.")

            setloading(true)
            setsearchResultFound(true)
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            const searchRemain = await Axios.post(`https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/hashtag/remove/credit`, {
                id: localStorage.getItem('organizationId')
            }, config)

            const res = await Axios.get(`${constant.url}tags/search?query=${category}`, config)
            settags(res.data)
            setloading(false)

            if (res.data.length === 0) {
                setsearchResultFound(false)
            } else {
                setsearchResultFound(true)
            }

            if (searchRemain.data.message === "unlimited") return
            context.updateCredites(searchRemain.data.message)
        } catch (error) {
            toast(error.response.data.error)
            setsearchResultFound(true)
            setloading(false)
        }
    }

    const getInstagramHashtag = async () => {
        try {
            if (!context.canEdit) return toast("You have only view permission")
            if (category === "") return toast("Please enter keywords to search")

            setloading(true)
            const workspaceId = localStorage.getItem("organizationId")
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            await Axios.post(`https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/hashtag/remove/credit/instagram`, {
                id: workspaceId
            }, config)

            const res = await Axios.get(`https://api.cache.creatosaurus.io/cache/hashtag/instagram/hashtag/${searchAccount}/${workspaceId}?query=${category}`, config)
            setinstaTags(res.data.hashTags)
            setloading(false)
        } catch (error) {
            if (error.response.data.error === "credit over") {
                toast("You don't have any credits to search hashtags, please upgrade to search more.")
            } else if (error.response.data.error === "Account not found") {
                setsearchAccount("")
                context.getInstagramAccounts()
                toast.error("You may have removed your account from our system. Please reconnect your account in order to continue searching hastags.")
            }
            setloading(false)
        }
    }

    const handelKeyDown = (e) => {
        if (e.key === "Enter") {
            if (searchAccount === "" && searchActiveButton === 1) return toast("Select instagram account to search hashtag")
            if (searchActiveButton === 1) {
                getInstagramHashtag()
            } else {
                find()
            }
        }
    }

    const getTheLoneLivedAccessToken = async (facebookUser) => {
        try {
            let user = facebookUser;
            let res = await Axios.get(`https://graph.facebook.com/v10.0/oauth/access_token?  
            grant_type=fb_exchange_token&          
            client_id=957071665735231&
            client_secret=769f5b546f0d0698f17d861bfa19d3a4&
            fb_exchange_token=${user.accessToken}`);
            user.accessToken = res.data.access_token;
            connectToInstagram(user)
        } catch (error) {
            setloading(false)
        }
    };


    const connectToInstagram = async (facebookUser) => {
        try {
            const res = await Axios.get(`https://graph.facebook.com/${facebookUser.userID}/accounts?fields=name,access_token,picture,is_owned&limit=100&access_token=${facebookUser.accessToken}`);

            const instagramPages = res.data.data;

            for (const data of instagramPages) {
                const response = await Axios.get(`https://graph.facebook.com/v10.0/${data.id}?fields=instagram_business_account&access_token=${data.access_token}`);
                if (response.data.instagram_business_account !== undefined) {
                    data.instagram_id = response.data.instagram_business_account.id;
                }
            }

            const filterThePagesHasInstagramId = instagramPages.filter((data) => data.instagram_id !== undefined);

            let filterInstagram = filterThePagesHasInstagramId.map(async (data) => {
                let res = await Axios.get(`https://graph.facebook.com/v10.0/${data.instagram_id}?fields=profile_picture_url,username&access_token=${data.access_token}`);

                let errorMsg = '';

                try {
                    await Axios.post(`https://graph.facebook.com/${data.instagram_id}/media`, {
                        image_url: '',
                        caption: '',
                        access_token: data.access_token,
                    });
                } catch (error) {
                    errorMsg = error.response.data.error.message;
                }

                if (errorMsg !== '(#10) The user is not an Instagram Business') {
                    let obj = {
                        accountType: "instagram",
                        facebookUserId: facebookUser.userID,
                        socialId: data.instagram_id,
                        profileURL: res.data.profile_picture_url,
                        name: res.data.username,
                        accessToken: data.access_token,
                        refreshToken: null,
                        active: false,
                        accessTokenExpiresIn: null,
                        refreshTokenExpiresIn: null
                    }
                    return obj;
                } else {
                    return null
                }
            });

            filterInstagram = await Promise.all(filterInstagram);

            // remove the null accounts from the list
            filterInstagram = filterInstagram.filter(data => data !== null)


            context.instagram.forEach(data2 => {
                filterInstagram.forEach(data => {
                    if (data.socialId === data2.socialId) {
                        data.active = true
                        data.hide = true
                    }
                })
            })

            setpopupActive(true)
            setaccountsLoading(false)
            setinstagramAccounts(filterInstagram)
        } catch (error) {
            setaccountsLoading(false)
        }
    };

    const instagramLogin = () => {
        window.FB.login((response) => {
            const facebookUser = response.authResponse;
            if (facebookUser === null) {
                setaccountsLoading(false)
                toast.error("Failed to authenticate the user please try again.")
            } else {
                getTheLoneLivedAccessToken(facebookUser);
            }
        },
            {
                scope: [
                    'public_profile',
                    'pages_show_list',
                    'instagram_content_publish',
                    'instagram_basic',
                ],
            }
        );
    }

    const login = () => {
        setShowInstaPopup(false)
        if (!context.canEdit) return toast("You have only view permission")

        setaccountsLoading(true)
        window.FB.getLoginStatus(response => {
            if (response.status === 'connected') {
                window.FB.logout(() => {
                    instagramLogin()
                })
            } else {
                instagramLogin()
            }
        })
    }

    const instagramAccount = (value) => {
        if (value === "connect") {
            setShowInstaPopup(true)
        } else {
            setsearchAccount(value)
        }
    }

    const close = (value) => {
        setpopupActive(false)
    }

    const toggle = (i) => {
        let filterData = instagramAccounts.map((data, index) => {
            if (index === i) {
                data.active = !data.active
            }
            return data
        })
        setinstagramAccounts(filterData)
    }

    const copyTags = (tags) => {
        let copyText = document.getElementById("copySaved");
        let concatTags
        tags.forEach((data, index) => {
            if (index === 0) {
                concatTags = data
            } else {
                concatTags = concatTags + " " + data
            }
        })
        copyText.value = concatTags
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Tag's copied", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const copyTag = (tag) => {
        let copyText = document.getElementById("copySaved");
        copyText.value = tag
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        toast("Tag copied", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const LoadingNew = ({ saved }) => {
        return <div className='scroll-container'>
            {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map(data => {
                    return <div key={data} className="loading-card">
                        <div className='skeleton' style={{ width: saved ? "50%" : "100%", height: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        {
                            saved ? <>
                                <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                                <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                            </> : null
                        }
                    </div>
                })
            }
        </div>
    }

    const closeInstaPopup = () => {
        setShowInstaPopup(false)
    }

    return (
        <React.Fragment>
            {accountsLoading ? <Loading /> : null}
            {showInstaPopup ? <InsagramConnectPopup close={closeInstaPopup} login={login} /> : null}
            {popupActive ? <ConnectAccountPopup toggle={toggle} close={close} data={instagramAccounts} instagramAccount={instagramAccount} /> : null}
            <input id="copySaved" type="text" value="" readOnly={true} />

            <div className="create-category">
                <div className='tabs-container'>
                    <div className={activeButton === 1 ? "active" : null} onClick={() => setactiveButton(1)}>Search</div>
                    <div className={activeButton === 2 ? "active" : null} onClick={() => setactiveButton(2)}>Saved</div>
                    {/**<div className={activeButton === 3 ? "active" : null} onClick={() => setactiveButton(3)}>Twitter Trends</div> */}
                </div>

                {
                    activeButton === 1 ?
                        <React.Fragment>
                            <div className='button-container'>
                                <button
                                    onClick={() => setsearchActiveButton(1)}
                                    className={searchActiveButton === 1 ? "active" : null}>Instagram tags</button>
                                <button
                                style={{display:'none'}}
                                    onClick={() => setsearchActiveButton(2)}
                                    className={searchActiveButton === 2 ? "active" : null}>Twitter tags</button>
                            </div>
                            {
                                searchActiveButton === 1 ? <React.Fragment>
                                    {
                                        context.instagramAccountsLoading ? <div className='loader' style={{ marginTop: 10 }} /> :
                                            <select
                                                value={searchAccount}
                                                onChange={(e) => instagramAccount(e.target.value)}>
                                                <option value="" disabled defaultValue>Select Instagram bussiness</option>
                                                {
                                                    context.instagram.map(data => {
                                                        return <option key={data.socialId} value={data.socialId}>{data.name}</option>
                                                    })
                                                }
                                                <option value="connect">Connect new Instagram bussiness account</option>
                                            </select>
                                    }
                                </React.Fragment> : <select
                                    style={{ display: 'none' }}
                                    value={searchAccount}
                                    onChange={(e) => setsearchAccount(e.target.value)}>
                                    <option value="" disabled defaultValue>Select Instagram account</option>
                                    <option value="connect">Connect new Instagram bussiness account</option>
                                </select>
                            }

                            <div className='input-container'>
                                <input value={category}
                                    maxLength={30}
                                    placeholder="Search words & keywords"
                                    onKeyDown={handelKeyDown}
                                    onChange={(e) => setcategory(e.target.value)} />
                                <button style={searchAccount === "" && searchActiveButton === 1 ? { backgroundColor: 'rgba(229, 229, 229, 0.5)' } : null} onClick={() => searchAccount === "" && searchActiveButton === 1 ? toast("Select instagram account to search hashtag") : searchActiveButton === 1 ? getInstagramHashtag() : find()}>
                                    {
                                        loading ? <div className='loader' style={{ height: 18, width: 18, borderWidth: 2 }} /> :
                                            <React.Fragment>
                                                {
                                                    searchAccount === "" && searchActiveButton === 1 ?
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M15.5 15.5L19 19" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M5 11C5 14.3137 7.68629 17 11 17C12.6597 17 14.1621 16.3261 15.2483 15.237C16.3308 14.1517 17 12.654 17 11C17 7.68629 14.3137 5 11 5C7.68629 5 5 7.68629 5 11Z" stroke="#808080" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>

                                                        : <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" strokeWidth="1.5" viewBox="0 0 24 24" fill="none">
                                                            <path d="M15.5 15.5L19 19" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                            <path d="M5 11C5 14.3137 7.68629 17 11 17C12.6597 17 14.1621 16.3261 15.2483 15.237C16.3308 14.1517 17 12.654 17 11C17 7.68629 14.3137 5 11 5C7.68629 5 5 7.68629 5 11Z" stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" />
                                                        </svg>
                                                }
                                            </React.Fragment>
                                    }
                                </button>
                            </div>
                            <div className='show-tags'>
                                {
                                    searchActiveButton === 1 ? <React.Fragment>
                                        {
                                            instaTags.map((data, index) => {
                                                return <InstaTagsCard copyTag={copyTag} key={data.tag + index} data={data} />
                                            })
                                        }
                                    </React.Fragment> : <React.Fragment>
                                        {
                                            searchResultFound ? tags.map((data, index) => {
                                                return <TagsCard copyTag={copyTag} key={index} data={data} />
                                            }) : <div style={{ fontSize: 14, marginTop: 10 }}>No search results were found. Please check your keyword or spelling and try again.</div>
                                        }
                                    </React.Fragment>
                                }
                            </div>
                        </React.Fragment> : activeButton === 2 ?
                            <div className='card-wrapper'>
                                <select
                                    value={context.filterCategory}
                                    onChange={(e) => context.changeFilterCategory(e.target.value)}>
                                    <option value="">All</option>
                                    {
                                        context.categoryData.map(data => {
                                            return <option key={data._id} value={data.category}>{data.category}</option>
                                        })
                                    }
                                </select>
                                {
                                    context.savedHashTagsLoading ? <LoadingNew saved={true} /> :
                                        context.savedHashTags.map((data, index) => {
                                            if (context.filterCategory === "") {
                                                return <div style={{ cursor: 'pointer' }} className="card" key={index + "home-recent"} onClick={() => copyTags(data.tags)}>
                                                    <p>{data.groupName}</p>
                                                    <span>{data.tags.map(data => {
                                                        return data + " "
                                                    })}</span>
                                                </div>
                                            } else if (context.filterCategory === data.category) {
                                                return <div style={{ cursor: 'pointer' }} className="card" key={index + "home-recent"} onClick={() => copyTags(data.tags)}>
                                                    <p>{data.groupName}</p>
                                                    <span>{data.tags.map(data => {
                                                        return data + " "
                                                    })}</span>
                                                </div>
                                            } else {
                                                return null
                                            }
                                        })
                                }
                            </div> : <div className='twitter-trends'>
                                <select value={context.trendCode} onChange={(e) => context.changeTrendCode(e.target.value)}>
                                    <option value="1">World Wide</option>
                                    <option value="23424848">India</option>
                                    <option value="23424977">US</option>
                                    <option value="23424748">Australia</option>
                                    <option value="23424975">UK</option>
                                    <option value="23424775">Canada</option>
                                </select>
                                <div className='scroll'>
                                    {
                                        context.trendingLoading ? <LoadingNew saved={false} /> :
                                            <React.Fragment>
                                                {
                                                    context.trending.map((data, index) => {
                                                        return <div style={{ cursor: 'pointer' }} key={index} className='card' onClick={() => copyTag(data.name)}>
                                                            <span>{data.name}</span>
                                                        </div>
                                                    })
                                                }
                                            </React.Fragment>
                                    }
                                </div>
                            </div>
                }
            </div>
        </React.Fragment>
    )
}

export default RightCreateCategory
