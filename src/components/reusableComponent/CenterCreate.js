import React, { useContext, useRef, useState, useEffect, useCallback } from 'react'
import '../reusableComponentCss/CenterCreate.css'
import AppContext from '../../store/DataProvider'
import decodeToken from "jwt-decode";
import Axios from 'axios'
import Editor, { createEditorStateWithText } from '@draft-js-plugins/editor';
import createHashtagPlugin from '@draft-js-plugins/hashtag';
import 'draft-js/dist/Draft.css';
import editorStyles from '../reusableComponentCss/HashtagStyle.module.css';
import constant from '../../constant';
import CreateCategoryPopup from './CreateCategoryPopup';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const hashtagPlugin = createHashtagPlugin({ theme: editorStyles });
const plugins = [hashtagPlugin];

const CenterCreate = () => {

    const inputHashtag = useRef(null)
    const [loading, setloading] = useState(false)
    const context = useContext(AppContext)
    const [textData, settextData] = useState(createEditorStateWithText(context.tagsValueEdit))

    const editState = useCallback(() => {
        context.changeTagsValueEdit("") // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    useEffect(() => {
        return () => {
            editState()
        }
    }, [editState])

    const updateTags = async () => {
        if (!context.canEdit) return toast("You have only view permission")
        if (context.groupName === null || context.groupName === "") return toast("Please enter title")
        if (context.tags.length === 0) return toast("Please enter #tags")
        if (context.category === "") return toast("Please select category")

        const decode = decodeToken(localStorage.getItem("token"))
        let organizationId = localStorage.getItem('organizationId')
        setloading(true)

        await Axios.put(constant.url, {
            id: context.editId,
            organizationId: organizationId,
            userId: decode.id,
            userName: decode.userName,
            groupName: context.groupName.toLowerCase(),
            category: context.category.toLowerCase(),
            tags: context.tags
        })

        setloading(false)
        context.changeTags([])
        context.changeEditId(null)
        context.getTheSavedHashTags()
        settextData(createEditorStateWithText(""))
        toast("Updated successfully", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const saveTheTags = async () => {
        try {
            if (!context.canEdit) return toast("You have only view permission")
            if (context.groupName === null || context.groupName === "") return toast("Enter group name")
            if (context.tags.length === 0) return toast("Enter Tags")
            if (context.category === "") return toast("Please select category")
            const decode = decodeToken(localStorage.getItem("token"))
            let organizationId = localStorage.getItem('organizationId')
            setloading(true)

            const res = await Axios.post(constant.url, {
                organizationId: organizationId,
                userId: decode.id,
                userName: decode.userName,
                groupName: context.groupName.toLowerCase(),
                category: context.category.toLowerCase(),
                tags: context.tags
            })

            setloading(false)
            context.changeTags([])
            context.changeGroupName("")
            context.changeCategory("")
            context.updateSavedHashtag(res.data.data)
            settextData(createEditorStateWithText(""))
            toast("Saved successfully", {
                autoClose: 1000,
                hideProgressBar: false,
            });
        } catch (error) {
            setloading(false)
        }
    }


    const changeText = (value) => {
        settextData(value)
        let data = value.getCurrentContent().getPlainText('\u0001').replaceAll("\u0001", " ")
        data = data.split(" ")
        let filterTags = data.filter(data => data.startsWith("#"))
        filterTags = filterTags.slice(0, 30) // get only 30 #tags
        context.changeTags([...filterTags])
    }

    const categoryCreatedToast = () => {
        toast("Category created", {
            autoClose: 1000,
            hideProgressBar: false,
        });
    }

    const changeCategory = (e) => {
        if (!context.canEdit) return toast("You have only view permission")
        context.changeCategory(e.target.value)
    }

    return (
        <div className="center-create">
            {context.category === "create" ? <CreateCategoryPopup categoryCreatedToast={categoryCreatedToast} /> : null}
            <span className="title">Title<sup>*</sup></span>
            <input className="title-input" type="text"
                placeholder="Enter Title"
                value={context.groupName}
                maxLength={30}
                onChange={(e) => context.changeGroupName(e.target.value)} />

            <span className="title">#tags<sup>*</sup>
                <span title="The text which is followed by # will only get saved">
                    <svg width="10" height="10" viewBox="0 0 10 10" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M4.99967 9.16732C7.30084 9.16732 9.16634 7.30182 9.16634 5.00065C9.16634 2.69946 7.30084 0.833984 4.99967 0.833984C2.69849 0.833984 0.833008 2.69946 0.833008 5.00065C0.833008 7.30182 2.69849 9.16732 4.99967 9.16732Z" stroke="#808080" strokeWidth="0.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M3.75 3.75C3.75 2.29166 6.04167 2.29167 6.04167 3.75C6.04167 4.79167 5 4.5833 5 5.8333" stroke="#808080" strokeWidth="0.75" strokeLinecap="round" strokeLinejoin="round" />
                        <path d="M5 7.50411L5.0037 7.5" stroke="#808080" strokeWidth="0.75" strokeLinecap="round" strokeLinejoin="round" />
                    </svg>
                </span>
            </span>
            <div className="text-area" onClick={() => inputHashtag.current.focus()}>
                <Editor
                    editorState={textData}
                    onChange={changeText}
                    plugins={plugins}
                    ref={inputHashtag}
                    placeholder="Enter your #tags"
                />
                <span className="length">{context.tags.length}</span>
            </div>
            <span className="title">Category<sup>*</sup></span>

            {
                context.categoryDataLoading ? <div className='loader' style={{ marginTop: 10 }} /> :
                    <select value={context.category}
                        style={context.category === "" ? { color: '#808080' } : null}
                        onChange={changeCategory}>
                        <option value="" disabled defaultValue>Select category</option>
                        {
                            context.categoryData.map(data => {
                                return <option key={data._id} value={data.category}>{data.category}</option>
                            })
                        }
                        <option value="create">Create new category</option>
                    </select>
            }

            {
                context.editId !== null ? <div>
                    <button onClick={updateTags} className='save'>{loading ? "Updating..." : "Update"}</button>
                    <button className='cancel' onClick={() => {
                        settextData(createEditorStateWithText(""))
                        context.changeEditId(null)
                        context.changeLeftSideBarAciveButton(3)
                    }}>Cancel</button>
                </div> :
                    <button className='save' onClick={saveTheTags}>{loading ? "Saving..." : "Save"}</button>
            }
            <ToastContainer />
        </div>
    )
}

export default CenterCreate
