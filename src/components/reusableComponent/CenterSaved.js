import React, { useContext } from 'react'
import AppContext from '../../store/DataProvider'
import '../reusableComponentCss/CenterSaved.css'
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import CenterSavedCard from '../card/CenterSavedCard'

const CenterSaved = () => {

    const context = useContext(AppContext)

    const Loading = () => {
        return <div className='scroll-container'>
            {
                [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20].map(data => {
                    return <div key={data} className="loading-card">
                        <div className='skeleton' style={{ width: "50%", height: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div className='skeleton' style={{ width: "100%", height: 10, marginTop: 10 }} />
                        <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                            <div>
                                <div className='skeleton' style={{ width: 200, height: 10, marginTop: 10 }} />
                                <div className='skeleton' style={{ width: 200, height: 10, marginTop: 10 }} />
                            </div>
                            <div style={{ display: 'flex', marginTop: 10 }}>
                                <div className='skeleton' style={{ width: 20, height: 10, marginRight: 10 }} />
                                <div className='skeleton' style={{ width: 20, height: 10, marginRight: 10 }} />
                                <div className='skeleton' style={{ width: 20, height: 10, marginRight: 10 }} />
                            </div>
                        </div>
                    </div>
                })
            }
        </div>
    }

    const handleScroll = (e) => {
        const { scrollTop, clientHeight, scrollHeight } = e.target
        if (scrollTop + clientHeight >= scrollHeight - 5) {
            if (context.savedFinished || context.savedHashTagsLoading) return
            context.getTheSavedHashTags()
        }
    };

    return (
        <div className="center-saved" onScroll={handleScroll}>
            <input id="copySaved" type="text" value="" readOnly={true} />
            {
                context.savedHashTags.length === 0 && context.savedHashTagsLoading !== true ? <div style={{ marginTop: 10, textAlign: 'center' }}>You haven't saved anything.</div> :
                    context.savedHashTags.map((data, index) => {
                        if (context.filterCategory === "") {
                            return <CenterSavedCard key={data._id + index} data={data} />
                        } else if (context.filterCategory === data.category) {
                            return <CenterSavedCard key={data._id + index} data={data} />
                        } else {
                            return null
                        }
                    })
            }

            {
                context.savedHashTagsLoading ? <Loading /> : null
            }
            <ToastContainer />
        </div>
    )
}

export default CenterSaved
