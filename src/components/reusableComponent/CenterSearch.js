import React, { useContext, useState } from 'react'
import '../reusableComponentCss/CenterSearch.css'
import AppContext from '../../store/DataProvider'
import InsagramConnectPopup from '../popup/InsagramConnectPopup/InsagramConnectPopup'
import ConnectAccountPopup from './ConnectAccountPopup'
import Loading from './Loading'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios'
import constant from '../../constant'

const CenterSearch = () => {
    const context = useContext(AppContext)
    const [showInstaPopup, setShowInstaPopup] = useState(false)
    const [searchAccount, setsearchAccount] = useState("")
    const [popupActive, setpopupActive] = useState(false)
    const [accountsLoading, setaccountsLoading] = useState(false)
    const [instagramAccounts, setinstagramAccounts] = useState([])
    const [activeButton, setactiveButton] = useState(2)
    const [loading, setloading] = useState(false)
    const [instaTags, setinstaTags] = useState([])
    const [category, setcategory] = useState("")
    const [activeIndex, setactiveIndex] = useState(null)
    const [addToExistingCollection, setaddToExistingCollection] = useState(false)
    const [loadingIndex, setLoadingIndex] = useState(null)

    const instagramAccount = (value) => {
        if (value === "connect") {
            setShowInstaPopup(true)
        } else {
            setsearchAccount(value)
        }
    }


    const getTheLoneLivedAccessToken = async (facebookUser) => {
        try {
            let user = facebookUser;
            let res = await axios.get(`https://graph.facebook.com/v10.0/oauth/access_token?  
            grant_type=fb_exchange_token&          
            client_id=957071665735231&
            client_secret=769f5b546f0d0698f17d861bfa19d3a4&
            fb_exchange_token=${user.accessToken}`);
            user.accessToken = res.data.access_token;
            connectToInstagram(user)
        } catch (error) {
            setaccountsLoading(false)
        }
    };

    const instagramLogin = () => {
        window.FB.login((response) => {
            const facebookUser = response.authResponse;
            if (facebookUser === null) {
                setaccountsLoading(false)
                toast.error("Failed to authenticate the user please try again.")
            } else {
                getTheLoneLivedAccessToken(facebookUser);
            }
        },
            {
                scope: [
                    'public_profile',
                    'pages_show_list',
                    'instagram_content_publish',
                    'instagram_basic',
                ],
            }
        );
    }

    const connectToInstagram = async (facebookUser) => {
        try {
            const res = await axios.get(`https://graph.facebook.com/${facebookUser.userID}/accounts?fields=name,access_token,picture,is_owned&limit=100&access_token=${facebookUser.accessToken}`);

            const instagramPages = res.data.data;

            for (const data of instagramPages) {
                const response = await axios.get(`https://graph.facebook.com/v10.0/${data.id}?fields=instagram_business_account&access_token=${data.access_token}`);
                if (response.data.instagram_business_account !== undefined) {
                    data.instagram_id = response.data.instagram_business_account.id;
                }
            }

            const filterThePagesHasInstagramId = instagramPages.filter((data) => data.instagram_id !== undefined);

            let filterInstagram = filterThePagesHasInstagramId.map(async (data) => {
                let res = await axios.get(`https://graph.facebook.com/v10.0/${data.instagram_id}?fields=profile_picture_url,username&access_token=${data.access_token}`);

                let errorMsg = '';

                try {
                    await axios.post(`https://graph.facebook.com/${data.instagram_id}/media`, {
                        image_url: '',
                        caption: '',
                        access_token: data.access_token,
                    });
                } catch (error) {
                    errorMsg = error.response.data.error.message;
                }

                if (errorMsg !== '(#10) The user is not an Instagram Business') {
                    let obj = {
                        accountType: "instagram",
                        facebookUserId: facebookUser.userID,
                        socialId: data.instagram_id,
                        profileURL: res.data.profile_picture_url,
                        name: res.data.username,
                        accessToken: data.access_token,
                        refreshToken: null,
                        active: false,
                        accessTokenExpiresIn: null,
                        refreshTokenExpiresIn: null
                    }
                    return obj;
                } else {
                    return null
                }
            });

            filterInstagram = await Promise.all(filterInstagram);

            // remove the null accounts from the list
            filterInstagram = filterInstagram.filter(data => data !== null)


            context.instagram.forEach(data2 => {
                filterInstagram.forEach(data => {
                    if (data.socialId === data2.socialId) {
                        data.active = true
                        data.hide = true
                    }
                })
            })

            setpopupActive(true)
            setaccountsLoading(false)
            setinstagramAccounts(filterInstagram)
        } catch (error) {
            setaccountsLoading(false)
        }
    };

    const login = () => {
        setShowInstaPopup(false)
        if (!context.canEdit) return toast("You have only view permission")

        setaccountsLoading(true)
        window.FB.getLoginStatus(response => {
            if (response.status === 'connected') {
                window.FB.logout(() => {
                    instagramLogin()
                })
            } else {
                instagramLogin()
            }
        })
    }

    const closeInstaPopup = () => {
        setShowInstaPopup(false)
    }

    const toggle = (i) => {
        let filterData = instagramAccounts.map((data, index) => {
            if (index === i) {
                data.active = !data.active
            }
            return data
        })
        setinstagramAccounts(filterData)
    }

    const close = () => {
        setpopupActive(false)
    }

    const getInstagramHashtag = async () => {
        try {
            if (searchAccount === "") return toast("Select instagram account to search hashtag")
            if (!context.canEdit) return toast("You have only view permission")
            if (category === "") return toast("Please enter keywords to search")

            setloading(true)
            setinstaTags([])
            const workspaceId = localStorage.getItem("organizationId")
            const token = localStorage.getItem('token');
            const config = { headers: { "Authorization": `Bearer ${token}` } }

            await axios.post(`https://api.app.creatosaurus.io/creatosaurus/userfeaturefactory/hashtag/remove/credit/instagram`, {
                id: workspaceId
            }, config)

            const res = await axios.get(`https://api.cache.creatosaurus.io/cache/hashtag/instagram/hashtag/${searchAccount}/${workspaceId}?query=${category}`, config)
            setinstaTags(res.data.hashTags)
            setloading(false)
        } catch (error) {
            if (error.response.data.error === "credit over") {
                toast("You don't have any credits to search hashtags, please upgrade to search more.")
            } else if (error.response.data.error === "Account not found") {
                setsearchAccount("")
                context.getInstagramAccounts()
                toast.error("You may have removed your account from our system. Please reconnect your account in order to continue searching hastags.")
            }
            setloading(false)
        }
    }

    const handelKeyDown = (e) => {
        if (e.key !== "Enter") return
        getInstagramHashtag()
    }

    const filterForTheCompition = () => {
        let obj = [
            {
                category: "Low Competition",
                class: "low",
                data: []
            }, {
                category: "Medium Competition",
                class: "medium",
                data: []
            },
            {
                category: "High Competition",
                class: "high",
                data: []
            }
        ]

        instaTags.forEach(data => {
            if (data.count >= 20) {
                obj[2].data.push(data)
            } else if (data.count >= 5) {
                obj[1].data.push(data)
            } else {
                obj[0].data.push(data)
            }
        })

        return obj
    }

    const changeActiveIndex = (value) => {
        if (value === activeIndex) {
            setactiveIndex(null)
            setaddToExistingCollection(false)
        } else {
            setactiveIndex(value)
            setaddToExistingCollection(false)
        }
    }

    const changeActiveButton = (value) => {
        setactiveButton(value)
        setactiveIndex(null)
        setaddToExistingCollection(false)
    }

    const addHashtag = async (tag, id, index) => {
        try {
            setLoadingIndex(index)
            setactiveIndex(null)
            setaddToExistingCollection(false)

            await axios.post(constant.url + "/title/update", {
                id: id,
                tag: tag
            })

            setLoadingIndex(null)
            toast.success("Hashtag added to collection")
        } catch (error) {
            setLoadingIndex(null)
            toast.success("Failed to add tag")
        }
    }

    return (
        <>
            {accountsLoading ? <Loading /> : null}
            {showInstaPopup ? <InsagramConnectPopup close={closeInstaPopup} login={login} /> : null}
            {popupActive ? <ConnectAccountPopup toggle={toggle} close={close} data={instagramAccounts} instagramAccount={instagramAccount} /> : null}
            <div className='center-search-container'>
                <div className='top-bar'>
                    <div className='head'>
                        <button>Instagram</button>
                        {
                            context.instagramAccountsLoading ?
                                <div className='loading-container'>
                                    <span className='loader' style={{ borderColor: '#fff', borderTopColor: '#FF43CA', height: 20, width: 20 }} />
                                </div> :
                                <select
                                    value={searchAccount}
                                    onChange={(e) => instagramAccount(e.target.value)}>
                                    <option className='loader' value="" disabled defaultValue>Select Instagram bussiness</option>
                                    {
                                        context.instagram.map(data => {
                                            return <option key={data.socialId} value={data.socialId}>{data.name}</option>
                                        })
                                    }
                                    <option value="connect">Connect new Instagram bussiness account</option>
                                </select>
                        }
                        <div className='input-wrapper'>
                            <input
                                value={category}
                                type='text'
                                placeholder='Search for hashtags'
                                onKeyDown={handelKeyDown}
                                onChange={(e) => setcategory(e.target.value)} />

                            <svg onClick={getInstagramHashtag} width="19" height="19" viewBox="0 0 19 19" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M12.2705 13.8535L15.0413 16.6243" stroke="#808080" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                                <path d="M3.95801 10.291C3.95801 12.9144 6.08465 15.041 8.70801 15.041C10.0219 15.041 11.2113 14.5075 12.0712 13.6453C12.9282 12.7861 13.458 11.6004 13.458 10.291C13.458 7.66766 11.3314 5.54102 8.70801 5.54102C6.08465 5.54102 3.95801 7.66766 3.95801 10.291Z" stroke="#808080" strokeWidth="1.25" strokeLinecap="round" strokeLinejoin="round" />
                            </svg>
                        </div>
                    </div>

                    <div className='button-container'>
                        <button onClick={() => changeActiveButton(1)} className={activeButton === 1 ? "active" : null}>Grouped</button>
                        <button onClick={() => changeActiveButton(2)} className={activeButton === 2 ? "active" : null}>Table</button>
                    </div>
                </div>


                {
                    loading ? <div className='loading-container-hashtag'>
                        <span className='loader' style={{ height: 50, width: 50, borderWidth: 5 }} />
                    </div> : instaTags.length === 0 ? null :
                        activeButton === 2 ? <>
                            <div className='table-hastag-container'>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>#Tags</th>
                                            <th>Competition</th>
                                            <th>Avg. Likes</th>
                                            <th>Avg. Comments</th>
                                            <th>More</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            instaTags.map((data, index) => {
                                                return <tr key={index + data.tag}>
                                                    <td>{data.tag}</td>
                                                    <td>
                                                        <div className='graph-container'>
                                                            <progress value={data.count} max={50} />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className='graph-container'>
                                                            <progress value={data.avgLike} max={data.max_likes} />
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div className='graph-container'>
                                                            <progress value={data.avgComment} max={data.max_comments} />
                                                        </div>
                                                    </td>
                                                    <td className='table-card'>
                                                        {
                                                            index === loadingIndex ?
                                                                <span className='loader' style={{ width: 28, height: 28, display: 'block' }} /> :
                                                                <svg style={{ cursor: 'pointer' }} onClick={() => changeActiveIndex(index)} width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M18 12.5C18.1326 12.5 18.2598 12.4473 18.3536 12.3536C18.4473 12.2598 18.5 12.1326 18.5 12C18.5 11.8674 18.4473 11.7402 18.3536 11.6464C18.2598 11.5527 18.1326 11.5 18 11.5C17.8674 11.5 17.7402 11.5527 17.6464 11.6464C17.5527 11.7402 17.5 11.8674 17.5 12C17.5 12.1326 17.5527 12.2598 17.6464 12.3536C17.7402 12.4473 17.8674 12.5 18 12.5ZM12 12.5C12.1326 12.5 12.2598 12.4473 12.3536 12.3536C12.4473 12.2598 12.5 12.1326 12.5 12C12.5 11.8674 12.4473 11.7402 12.3536 11.6464C12.2598 11.5527 12.1326 11.5 12 11.5C11.8674 11.5 11.7402 11.5527 11.6464 11.6464C11.5527 11.7402 11.5 11.8674 11.5 12C11.5 12.1326 11.5527 12.2598 11.6464 12.3536C11.7402 12.4473 11.8674 12.5 12 12.5ZM6 12.5C6.13261 12.5 6.25979 12.4473 6.35355 12.3536C6.44732 12.2598 6.5 12.1326 6.5 12C6.5 11.8674 6.44732 11.7402 6.35355 11.6464C6.25979 11.5527 6.13261 11.5 6 11.5C5.86739 11.5 5.74021 11.5527 5.64645 11.6464C5.55268 11.7402 5.5 11.8674 5.5 12C5.5 12.1326 5.55268 12.2598 5.64645 12.3536C5.74021 12.4473 5.86739 12.5 6 12.5Z" fill="black" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                                                </svg>
                                                        }

                                                        {
                                                            index === activeIndex ?
                                                                <div className='sub-card'>
                                                                    <button onClick={() => setaddToExistingCollection((prev) => !prev)}>Add to existing collection</button>
                                                                    <a target="_blank" rel="noreferrer" href={`https://www.instagram.com/explore/tags/${data.tag.substring(1)}`}>Preview On Instagram</a>
                                                                </div> : null
                                                        }

                                                        {
                                                            index === activeIndex && addToExistingCollection ?
                                                                <div className='titles-card'>
                                                                    {
                                                                        context.titles.length === 0 ? <>
                                                                            <span>No Collection found</span>
                                                                        </> :
                                                                            context.titles.map(title => {
                                                                                return <span onClick={() => addHashtag(data.tag, title._id, index)} key={title._id}>{title.groupName}</span>
                                                                            })
                                                                    }
                                                                </div> : null
                                                        }
                                                    </td>
                                                </tr>
                                            })
                                        }
                                    </tbody>
                                </table>
                            </div>
                        </> :
                            <div className='group-hastag-container'>
                                {
                                    filterForTheCompition().map((data, index) => {
                                        return <div key={index}>
                                            <h4 style={index === 0 ? { marginTop: 0 } : null}>{data.category}</h4>
                                            <div className='wrap'>
                                                {
                                                    data.data.map((tag, index1) => {
                                                        return <div key={tag + index} className={`card  ${data.class}`}>
                                                            <span>{tag.tag}</span>
                                                            {
                                                                loadingIndex === index + "" + index1 ?
                                                                    <span className='loader' style={{ marginLeft: 35, height: 15, width: 15, borderWidth: 1, borderColor: "#fff", borderTopColor: "#FF43CA" }} /> :
                                                                    <div className='rect' onClick={() => changeActiveIndex(index + "" + index1)}>
                                                                        <svg width="16" height="4" viewBox="0 0 16 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                            <path d="M14 2.5C14.1326 2.5 14.2598 2.44732 14.3536 2.35355C14.4473 2.25979 14.5 2.13261 14.5 2C14.5 1.86739 14.4473 1.74021 14.3536 1.64645C14.2598 1.55268 14.1326 1.5 14 1.5C13.8674 1.5 13.7402 1.55268 13.6464 1.64645C13.5527 1.74021 13.5 1.86739 13.5 2C13.5 2.13261 13.5527 2.25979 13.6464 2.35355C13.7402 2.44732 13.8674 2.5 14 2.5ZM8 2.5C8.13261 2.5 8.25979 2.44732 8.35355 2.35355C8.44732 2.25979 8.5 2.13261 8.5 2C8.5 1.86739 8.44732 1.74021 8.35355 1.64645C8.25979 1.55268 8.13261 1.5 8 1.5C7.86739 1.5 7.74021 1.55268 7.64645 1.64645C7.55268 1.74021 7.5 1.86739 7.5 2C7.5 2.13261 7.55268 2.25979 7.64645 2.35355C7.74021 2.44732 7.86739 2.5 8 2.5ZM2 2.5C2.13261 2.5 2.25979 2.44732 2.35355 2.35355C2.44732 2.25979 2.5 2.13261 2.5 2C2.5 1.86739 2.44732 1.74021 2.35355 1.64645C2.25979 1.55268 2.13261 1.5 2 1.5C1.86739 1.5 1.74021 1.55268 1.64645 1.64645C1.55268 1.74021 1.5 1.86739 1.5 2C1.5 2.13261 1.55268 2.25979 1.64645 2.35355C1.74021 2.44732 1.86739 2.5 2 2.5Z" fill="black" stroke="black" strokeWidth="1.5" strokeLinecap="round" strokeLinejoin="round" />
                                                                        </svg>
                                                                    </div>
                                                            }

                                                            {
                                                                index + "" + index1 === activeIndex ?
                                                                    <div className='sub-card'>
                                                                        <button onClick={() => setaddToExistingCollection((prev) => !prev)}>Add to existing collection</button>
                                                                        <a target="_blank" rel="noreferrer" href={`https://www.instagram.com/explore/tags/${tag.tag.substring(1)}`}>Preview On Instagram</a>
                                                                    </div> : null
                                                            }

                                                            {
                                                                index + "" + index1 === activeIndex && addToExistingCollection ?
                                                                    <div className='titles-card'>
                                                                        {
                                                                            context.titles.length === 0 ? <>
                                                                                <span>No Collection found</span>
                                                                            </> :
                                                                                context.titles.map(data => {
                                                                                    return <span onClick={() => addHashtag(tag.tag, data._id, index + "" + index1)} key={data._id}>{data.groupName}</span>
                                                                                })
                                                                        }
                                                                    </div> : null
                                                            }
                                                        </div>
                                                    })
                                                }
                                            </div>
                                        </div>
                                    })
                                }
                            </div>
                }

                <ToastContainer />
            </div>
        </>
    )
}

export default CenterSearch