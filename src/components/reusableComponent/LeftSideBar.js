import React, { useContext } from 'react'
import '../reusableComponentCss/LeftSideBar.css'
import AppContext from '../../store/DataProvider'

const LeftSideBar = () => {

    const context = useContext(AppContext)

    const changeTheContainer = (number) => {
        context.changeLeftSideBarAciveButton(number)
        if (number !== 2) {
            context.changeEditId(null)
        }
    }

    return (
        <div className="left-side-bar">
            <div className={context.leftSideBarActiveButton === 1 ? "button-container active" : "button-container"}
                onClick={() => changeTheContainer(1)}>
                <span className="emoji rotate">👀</span>
                <span className="title">Dashboard</span>
            </div>

            <div className={context.leftSideBarActiveButton === 6 ? "button-container active" : "button-container"}
                onClick={() => changeTheContainer(6)}>
                <span className="emoji">🔎</span>
                <span className="title">Search</span>
            </div>

            <div className={context.leftSideBarActiveButton === 2 ? "button-container active" : "button-container"}
                onClick={() => changeTheContainer(2)}>
                <span className="emoji">📝</span>
                <span className="title">Create Collection</span>
            </div>

            <div className={context.leftSideBarActiveButton === 3 ? "button-container active" : "button-container"}
                onClick={() => changeTheContainer(3)}>
                <span className="emoji">💾</span>
                <span className="title">Saved</span>
            </div>

            <div className={context.leftSideBarActiveButton === 4 ? "button-container active" : "button-container"}
                onClick={() => changeTheContainer(4)}>
                <span className="emoji">💁🏻</span>
                <span className="title">Help</span>
            </div>

            <div className={context.leftSideBarActiveButton === 5 ? "button-container active" : "button-container"}
                onClick={() => changeTheContainer(5)}>
                <span className="emoji">⚙️</span>
                <span className="title">Settings</span>
            </div>
        </div>
    )
}

export default LeftSideBar
