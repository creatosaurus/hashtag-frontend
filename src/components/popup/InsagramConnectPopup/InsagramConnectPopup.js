import React from 'react'
import './InsagramConnectPopup.css'

const InsagramConnectPopup = ({ close, login }) => {

    const info = [{
        clickHere: false,
        p: 'For the moment only Instagram Business accounts that have been connected to a Facebook Page and are managed by you are supported.'
    }, {
        clickHere: 'https://help.instagram.com/502981923235522',
        p: 'To convert your Instagram Personal account to a Business account,'
    }, {
        clickHere: 'https://help.instagram.com/399237934150902',
        p: 'To connect your Facebook Page to your Instagram Business account,'
    }, {
        clickHere: '#1',
        p: 'Check out our Instagram Business account connection guide for more information,'
    }]


    return (
        <div className='login-popup-container' onClick={close}>
            <div className='info-container' onClick={(e) => e.stopPropagation()}>
                <div className='title-container'>
                    <h2>Connect Instagram Business Account</h2>

                    {/* Corss SVG Icon */}
                    <svg onClick={close} xmlns="http://www.w3.org/2000/svg" width="28" height="28" viewBox="0 0 36 36" data-testid="close-icon">
                        <path d="M28.5 9.62L26.38 7.5 18 15.88 9.62 7.5 7.5 9.62 15.88 18 7.5 26.38l2.12 2.12L18 20.12l8.38 8.38 2.12-2.12L20.12 18z" />
                    </svg>
                </div>

                {
                    info.map((data, index) => {
                        return <p key={index}>{data.p}
                            <React.Fragment>
                                {
                                    // checking in perticular paragraph link is there or not
                                    data.clickHere === false ?
                                        null :
                                        <a href={data.clickHere} target="_blank" rel="noreferrer"> click here.</a>
                                }
                            </React.Fragment>
                        </p>
                    })
                }

                <div className='button-container'>
                    <button onClick={close}>Cancel</button>
                    <button onClick={login}>Continue</button>
                </div>
            </div>
        </div>
    )
}

export default InsagramConnectPopup