import React from 'react'
import { BrowserRouter, Switch, Route } from 'react-router-dom'
import Home from './components/screens/Home'
import NotSupported from './components/screens/NotSupported'
import ProtectedRoutes from './components/screens/ProtectedRoutes'

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <ProtectedRoutes exact path="/" component={Home} />
        <Route exact path="/safari-not-supported" component={NotSupported} />
      </Switch>
    </BrowserRouter>
  )
}

export default App
